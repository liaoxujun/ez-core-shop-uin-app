/*
 * @FilePath: vite.config.ts
 * @Author: 小飞侠
 * @Date: 2024-09-18 09:07:03
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-10-08 15:15:23
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 写入你的描述
 */
import { defineConfig } from 'vite'
import uni from '@dcloudio/vite-plugin-uni'

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    // 开发阶段启用源码映射：https://uniapp.dcloud.net.cn/tutorial/migration-to-vue3.html#需主动开启-sourcemap
    sourcemap: process.env.NODE_ENV === 'development',
  },
  plugins: [uni()],
})
