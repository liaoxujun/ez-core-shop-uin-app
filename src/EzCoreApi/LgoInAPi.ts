/*
 * @FilePath: LgoInAPi.ts
 * @Author: 小飞侠
 * @Date: 2024-08-30 10:00:08
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-09-18 10:15:29
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 写入你的描述
 */
import { EZhttp } from '@/utils/EzCoreHttp'
import type { EzTokeNinfoResult, StringResult } from '@/types/EzApiBaseTypes'

export const getGetPoicyAPI = (key: string) => {
  return EZhttp<StringResult>({
    method: 'GET',
    url: `/api/EzCorePoicy/GetPoicy?keystr=${key}`,
  })
}

/**快速登录 */
export const LogInOnWxCode = (code: string) => {
  return EZhttp<EzTokeNinfoResult>({
    method: 'GET',
    url: `/api/LogIn/LogInOnWxCode?code=${code}`,
  })
}
/** 新用户登录 */
export const LogInCreateOnWxCode = (data: UserInfoIn) => {
  return EZhttp<EzTokeNinfoResult>({
    method: 'POST',
    url: `/api/LogIn/LogInCreatOnWxCode`,
    data: data,
  })
}

/**
 * 用户信息模型 小程序创建用户登录
 */
export interface UserInfoIn {
  /**
   * 头像
   */
  avatar?: string
  /**
   * 昵称
   */
  nickName?: string
  /**
   * 电话
   */
  phone?: string
  /**
   * 名称
   */
  name?: string
  /**
   * 密码
   */
  passWord?: string
  /**
   * 手机号换取码 Bindetphonenumber
   */
  bindetphonenumber: string
  /**
   * JsCode
   */
  jsCode: string
}
