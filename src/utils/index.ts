/*
 * @FilePath: index.ts
 * @Author: 小飞侠
 * @Date: 2024-09-17 09:38:09
 * @LastEditors:
 * @LastEditTime: 2024-09-18 22:10:54
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 写入你的描述
 */
/**
 * 日期格式化函数
 * @param date 日期对象
 * @param format 日期格式，默认为 YYYY-MM-DD HH:mm:ss
 */
export const formatDate = (date: Date, format = 'YYYY-MM-DD HH:mm:ss') => {
  // 获取年月日时分秒，通过 padStart 补 0
  const year = String(date.getFullYear())
  const month = String(date.getMonth() + 1).padStart(2, '0')
  const day = String(date.getDate()).padStart(2, '0')
  const hours = String(date.getHours()).padStart(2, '0')
  const minutes = String(date.getMinutes()).padStart(2, '0')
  const seconds = String(date.getSeconds()).padStart(2, '0')

  // 返回格式化后的结果
  return format
    .replace('YYYY', year)
    .replace('MM', month)
    .replace('DD', day)
    .replace('HH', hours)
    .replace('mm', minutes)
    .replace('ss', seconds)
}


export function IsValidImageUrl(url:string):boolean {
  // 正则表达式，验证URL的有效性
  const urlRegex = /^(http|https):\/\/[^ "]+$/;

  // 正则表达式，验证图片扩展名
  const imageExtensionRegex = /\.(jpg|jpeg|png|gif|bmp|webp)$/i;

  // 首先验证URL的有效性
  if (!urlRegex.test(url)) {
      return false;
  }

  // 然后验证图片扩展名
  if (imageExtensionRegex.test(url)) {
      return true;
  }

  return false;
}
