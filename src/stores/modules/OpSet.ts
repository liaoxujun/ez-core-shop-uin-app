/*
 * @FilePath: OpSet.ts
 * @Author: 小飞侠
 * @Date: 2024-09-13 11:53:56
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-10-08 15:32:06
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 写入你的描述
 */
import type { MOpSetting } from '@/types/EzApiBaseTypes'
import { defineStore } from 'pinia'
import { ref } from 'vue'

// 定义 Store
export const useMOpSettingStore = defineStore(
  'MOpSetting ',
  () => {
    // token形象
    const profile = ref<MOpSetting>()

    // 保存会员信息，登录时使用
    const setProfile = (val: MOpSetting) => {
      profile.value = val
    }

    // 清理会员信息，退出时使用
    const clearProfile = () => {
      profile.value = undefined
    }

    // 记得 return
    return {
      profile,
      setProfile,
      clearProfile,
    }
  }
  ,
  {
    // 网页端配置
    // persist: true,
    // 小程序端配置
    persist: {
      storage: {
        getItem(key) {
          return uni.getStorageSync(key)
        },
        setItem(key, value) {

          uni.setStorageSync(key, value)
        },
      },
    },
  },

)
