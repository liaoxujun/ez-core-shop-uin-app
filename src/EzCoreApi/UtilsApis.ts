/*
 * @FilePath: UtilsApis.ts
 * @Author: 小飞侠
 * @Date: 2024-09-15 15:12:30
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-09-25 16:38:40
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 写入你的描述
 */
import { EZhttp } from '@/utils/EzCoreHttp'
import type { EzSYSDictionaryOtherSetResult, StringResult } from '@/types/EzApiBaseTypes'

export const EZGetOtherSettings = () => {
  return EZhttp<EzSYSDictionaryOtherSetResult>({
    method: 'GET',
    url: `/api/Utility/GetOtherSettings`,
  })
}

export const FileUpData = (fileSt: string) => {
  return EZhttp<StringResult>({
    method: 'POST',
    url: `/api/Utility/UpdateFileBase64`,
    data: { fileBase64: fileSt },
  })
}

