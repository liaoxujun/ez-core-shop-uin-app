/*
 * @FilePath: profile.ts
 * @Author: 小飞侠
 * @Date: 2024-09-18 09:07:03
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-09-18 16:32:21
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 写入你的描述
 */
import type {  UserDataVm } from '@/types/member'
import { EZhttp } from '@/utils/EzCoreHttp'
import type { EzResult } from '@/types/EzApiBaseTypes'

/**
 * 获取个人信息
 */
export const getMemberProfileAPI = () => {
  return EZhttp<EzResult<UserDataVm>>({
    method: 'GET',
    url: '/api/User/GetUserData',
  })
}

/**
 * 修改个人信息
 * @param data 请求体参数
 */
export const putMemberProfileAPI = (data: UserDataVm) => {
  return EZhttp<EzResult<boolean>>({
    method: 'POST',
    url: '/api/User/SetUserData',
    data,
  })
}
