/*
 * @FilePath: Token.ts
 * @Author: 小飞侠
 * @Date: 2024-09-13 11:53:56
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-09-25 16:08:15
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 写入你的描述
 */
import type { TokenResult } from '@/types/Token'
import { defineStore } from 'pinia'
import { ref } from 'vue'

// 定义 Store
export const useTokenStore = defineStore(
  'token',
  () => {
    // token形象
    const profile = ref<TokenResult>()

    // 保存会员信息，登录时使用
    const setProfile = (val: TokenResult) => {
        console.log("setProfile",val);

      profile.value = val
    }

    // 清理会员信息，退出时使用
    const clearProfile = () => {
        console.log("clearProfile");
      profile.value = undefined
    }

    // 记得 return
    return {
      profile,
      setProfile,
      clearProfile,
    }
  }
  ,
  {
    // 网页端配置
    // persist: true,
    // 小程序端配置
    persist: {
      storage: {
        getItem(key) {
          return uni.getStorageSync(key)
        },
        setItem(key, value) {
          uni.setStorageSync(key, value)
        },
      },
    },
  },

)
