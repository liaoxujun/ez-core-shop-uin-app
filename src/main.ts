/*
 * @FilePath: main.ts
 * @Author: 小飞侠
 * @Date: 2024-09-18 09:07:03
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-10-08 13:19:41
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 写入你的描述
 */
import { createSSRApp } from 'vue'
import App from './App.vue'

// 导入 pinia 实例
import pinia from './stores'
export function createApp() {
  // 创建 vue 实例
  const app = createSSRApp(App)
  // 使用 pinia
  app.use(pinia)

  return {
    app,
  }
}
