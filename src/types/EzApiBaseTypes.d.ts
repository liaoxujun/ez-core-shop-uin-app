/*
 * @FilePath: EzApiBaseTypes.d.ts
 * @Author: 小飞侠
 * @Date: 2024-04-15 15:31:08
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-09-25 15:46:59
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: EzCore同意返回接口封装
 */
/**
 * 通用返回定义
 */
export interface EzResult<T> {
  /** 成EzCoreResult功状态 */
  code: number;
  /** 标准状态 */
  statusCode?: number | null;
  data?: T | null;
  /** 错误消息 */
  error?: any | null;
  /** 消息 */
  message: string | null;
  /** 附加值 */
  extras?: any | null;
  /** 时间戳 */
  timestamp: number | null;
}

/**
 * 类型基础，主要是定义创建人创建时间修改人修改时间，已经删除标志
 */
export interface EzBaseModel {
  /**
   * 创建人
   */
  createBy?: string | null;
  /**
   * 创建时间
   */
  createTime?: Date | null;
  /**
   * 修改时间
   */
  upDataTime?: Date | null;
  /**
   * 修改人
   */
  upDataBy?: string | null;
  /**
   * 删除标志
   */
  isDeleted?: boolean;
  /** 是否有效 */
  isEN?: boolean;
  /**
   * 排序
   */
  sort?: number | null;
}

/** 页面搜索基类 */
export interface EzPageModel {
  /** 页面号*/
  pageIndex: number;
  /** 每页行好设置 */
  pageSize: number;
  /** 总行数 */
  totalCount?: number;
}
/**boolean类型返回通用定义 */
export interface BoolResult extends EzResult<boolean> {}
/**boolean类型返回通用定义 */
export interface StringResult extends EzResult<string> {}
/** 区域坐标集合 */
export interface ZoneInfo {
  /**  区域坐标表集合  */
  coords: Array<GeoCoordinate>;
  /** 中心点坐标 */
  centrecoord?: GeoCoordinate;
  /** 地址 */
  address?: string;
  /** id */
  id?: string;
}
/** 经纬度坐标 */
export interface GeoCoordinate {
  /** 纬度 */
  latitude: number;
  /** 经度 */
  longitude: number;
}

/** 枚举属性列表类型 */
export interface EmunPrList
{
  /** key */
  key:string,
  /** 值 */
  value:number,
  /** 描述 */
  description:string
}
/**通用枚举返回属性 */
export interface EmunPrResult extends EzResult<EmunPrList[]> {}



/**  toke信息模型 */
export interface EzTokeNinfo
{
  /** `token` */
  accessToken: string;
  /** 用于调用刷新`accessToken`的接口时所需的`token` */
  refreshToken: string;
  /** `accessToken`的过期时间（格式'xxxx/xx/xx xx:xx:xx'） */
  expires: Date;
};
/** toke返回定义 */
export interface EzTokeNinfoResult extends EzResult<EzTokeNinfo> {}



/**
 * 其他设置
 */
export interface MOpSetting {
  /**
   * 腾讯地图Key
   */
  tenantMapKey: string;
  /**
   * 腾讯地图SK
   */
  tenantMapSK: string;
  /**
   * 分享图片
   */
  sharImg: string;
}
/** 其他设置返回定义 */
export interface EzSYSDictionaryOtherSetResult extends EzResult<MOpSetting> {}


/** 枚举属性列表类型 */
export interface EzEmunOptions
{
  /** key */
  key:string,
  /** 值 */
  value:number,
  /** 描述 */
  description:string
}