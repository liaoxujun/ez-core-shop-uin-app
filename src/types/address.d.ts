/*
 * @FilePath: address.d.ts
 * @Author: 小飞侠
 * @Date: 2024-09-18 09:07:03
 * @LastEditors:
 * @LastEditTime: 2024-09-29 10:46:26
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 写入你的描述
 */


/**
 * 地址详情VM
 */
export interface AddressMemberVm {
  /**
   * 用户ID
   */
  id: number;

  /**
   * 收货人名称
   */
  receiver?: string;

  /**
   * 联系方式
   */
  contact?: string;

  /**
   * 省市区（用于前端展示）
   */
  fullLocation?: string;

  /**
   * 省份编码（后端参数）
   */
  provinceCode?: string;
  /** 省份 */
  province?:string ;
  /**
   * 城市编码（后端参数）
   */
  cityCode?: string;
  /** 城市 */
  city?:string ;

  /**
   * 区/县编码（后端参数）
   */
  countyCode?: string;
  /** 区县 */
  county?:string ;
  /**
   * 详细地址
   */
  address?: string;

  /**
   * 是否为默认地址
   */
  isDefault?: boolean;

  /**
   * 地图坐标 - 经度
   */
  latitude?: number;

  /**
   * 地图坐标 - 纬度
   */
  longitude?: number;
}
