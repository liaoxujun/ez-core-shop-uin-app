/*
 * @FilePath: Token.d.ts
 * @Author: 小飞侠
 * @Date: 2024-09-13 11:53:17
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-09-15 15:37:18
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion:token
 */
/** 小程序登录 登录KEY */
export type TokenResult = {
  /** `token` */
  accessToken: string
  /** 用于调用刷新`accessToken`的接口时所需的`token` */
  refreshToken: string
  /** `accessToken`的过期时间（格式'xxxx/xx/xx xx:xx:xx'） */
  expires: Date
}
