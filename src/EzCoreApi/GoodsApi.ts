/*
 * @FilePath: GoodsApi.ts
 * @Author: 小飞侠
 * @Date: 2024-10-04 18:18:41
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-10-04 21:18:23
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 写入你的描述
 */
import { EZhttp } from "@/utils/EzCoreHttp";
import type {  EzEmunOptions, EzResult } from "@/types/EzApiBaseTypes";
import type { CategoryGoodsItemParamIn, GoodsItemDto, ezCategoryDto } from "@/types/EzGoods";
/** 获取商品分类列表 */
export const GetGoodsCategory = () => {
  return EZhttp<EzResult<ezCategoryDto[]>>({
    method: 'GET',
    url: `/api/EzGoods/GetGoodsCategory`,
  })
}
/** 获取销售方式 */
export const GetSaleMethodOptings = () => {
  return EZhttp<EzResult<EzEmunOptions[]>>({
    method: 'GET',
    url: `/api/EzGoods/GetSaleMethodOptings`,
  })
}
/**获取商品列表 */
export const GetCategoryGoods = (data : CategoryGoodsItemParamIn) => {
  return EZhttp<EzResult<GoodsItemDto[]>>({
    method: 'POST',
    url: `/api/EzGoods/GetCategoryGoods`,
    data:data
  })
}
