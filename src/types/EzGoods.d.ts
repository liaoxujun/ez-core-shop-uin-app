
/** 产品分类列表 */
export interface ezCategoryDto
{
  /**
     * ID
     */
  id: number;

  /**
   * 名称
   */
  name?: string | null;

  /**
   * logo图片
   */
  logoPath?: string | null;

  /**
   * 分类简介
   */
  info?: string | null;

  /**
   * 子路由集合，定义嵌套的路由结构
   */
  children?: ezCategoryDto[] | null;
}
/*获取商品列表输入模型*/
export interface CategoryGoodsItemParamIn {
  /**
   * ID
   */
  categoryId: number;

  /**
   * 页码
   */
  pageIndex: number;

  /**
   * 页数
   */
  pageSize: number;
}

/** 商品列表选项 */
export interface GoodsItemDto {
  /**
   * ID
   */
  id: number;

  /**
   * 名称
   */
  name?: string | null;

  /**
   * 是否热门
   */
  isHot: boolean;

  /**
   * 是否上架
   */
  isEn?: boolean | null;

  /**
   * 销量
   */
  sales?: number | null;

  /**
   * 是否推荐
   */
  isRecommend?: boolean | null;

  /**
   * 封面图
   */
  image?: string | null;

  /**
   * 商品简介
   */
  goodsDescription?: string | null;

  /**
   * 商品品牌名称
   */
  brandName?: string | null;

  /**
   * 商品 LOGO
   */
  brandImg?: string | null;

  /**
   * 销售价格
   */
  price?: number | null;

  /**
   * 市场价格
   */
  marketPrice?: number | null;

  /**
   * 售卖方式
   */
  leaseMod: string|null;
}
