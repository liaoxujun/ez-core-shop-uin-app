/*
 * @FilePath: member.d.ts
 * @Author: 小飞侠
 * @Date: 2024-09-15 15:20:25
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-09-20 13:58:20
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 个人详情存储
 */
/** 通用的用户信息 */
type BaseProfile = {
  /**
   * 性别
   */
  // sex?: GenderType
  sex?: "Male" | "Female";
  /**
   * 头像图片路径
   */
  avatarImage?: string | null

  /**
   * 用户昵称
   */
  nickName?: string | null


}

/** 小程序登录 登录用户信息 */
export type LoginResult = BaseProfile & {
  /** 手机号 */
  mobile: string
  /** 登录凭证 */
  token: string
}

/** 个人信息 用户详情信息 */
export type ProfileDetail = BaseProfile & {
  /**
   * 员工分组名或VIP等级名称
   */
  userGroupName?: string | null

  /**
   * 分组徽章图片路径
   */
  groupBadgeImgPath?: string | null


  /** 省市区 */
  fullLocation?: string
  /** 职业 */
  profession?: string
  // 生日
  birthday?: string | null;

}
/** 性别 */
export type Gender = '女' | '男'

/** 个人信息 修改请求体参数 */
export type ProfileParams = Pick<
  ProfileDetail,
  'nickname' | 'gender' | 'birthday' | 'profession'
> & {
  /** 省份编码 */
  provinceCode?: string
  /** 城市编码 */
  cityCode?: string
  /** 区/县编码 */
  countyCode?: string
}

// TypeScript接口定义
export type UserDataVm = {
  // 性别
  sex?: "Male" | "Female";
  // 头像
  avatarImage?: string | null;
  // 昵称
  nickName?: string | null;
  // 生日
  birthday?: string | null;
  /** 职业 */
  profession?: string;
  // 生日
  birthday?: Date | null;

  /** 省市区 */
  fullLocation?: string
  /** 职业 */
  profession?: string
}
/**
 * 性别枚举
 */
export enum GenderType {
  /**
   * 男性
   */
  Male,

  /**
   * 女性
   */
  Female,

  /**
   * 其他
   */
  Other,

  /**
   * 未设置
   */
  NotSpecified
}
