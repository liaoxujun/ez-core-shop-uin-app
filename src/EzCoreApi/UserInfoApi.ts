/*
 * @FilePath: UserInfoApi.ts
 * @Author: 小飞侠
 * @Date: 2024-09-11 11:40:28
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2024-09-29 13:05:43
 * Copyright: 2024  MinTinge CO.,LTD. All Rights Reserved.
 * @Descripttion: 用户详情接口
 */
import { EZhttp } from "@/utils/EzCoreHttp";
import type {  EzResult } from "@/types/EzApiBaseTypes";
import type { GenderType } from "@/types/member";
import type { AddressMemberVm } from "@/types/address";

export const GetUserInfo = () => {
    return EZhttp<EzResult<UserInfoVm>>({
      method: 'GET',
      url: `/api/User/GetUserInfo`
    })
  }
  export const GetUserData = () => {
    return EZhttp<EzResult<UserDataDto>>({
      method: 'GET',
      url: `/api/User/GetUserData`
    })
  }

  export const SetUserData = (data:UserDataDto) => {
    return EZhttp<EzResult<Boolean>>({
      method: 'POST',
      url: `/api/User/SetUserData`,
      data:{data}
    })
  }
  /** 获取地址列表 */
  export const GetAddressList = () => {
    return EZhttp<EzResult<AddressMemberVm[]>>({
      method: 'GET',
      url: `/api/User/GetAddressList`

    })
  }

    /** 获取地址详情 */
    export const GetAddressMember = ( id:number) => {
      return EZhttp<EzResult<AddressMemberVm>>({
        method: 'GET',
        url: `/api/User/GetAddressMember?id=${id}`,
      })
    }

     /** 设置地址
      * id 为0或者空时为创建
     */
     export const SetAddressMember = ( data:AddressMemberVm) => {
      return EZhttp<EzResult<Boolean>>({
        method: 'POST',
        url: `/api/User/SetAddressMember`,
        data

      })
    }
     /** 删除地址
      * id 为0或者空时为创建
     */
     export const DelAddress = ( id:string) => {
      return EZhttp<EzResult<Boolean>>({
        method: 'DELETE',
        url: `/api/User/DelAddressMember?id=${id}`,


      })
    }
  /**
 * 用户信息视图模型
 */
export interface UserInfoVm {
    /**
     * 性别
     */
    sex?: GenderType;
    /**
     * 头像图片路径
     */
    avatarImage?: string | null;
    /**
     * 用户昵称
     */
    nickName?: string | null;

    /**
     * 员工分组名或VIP等级名称
     */
    userGroupName?: string | null;

    /**
     * 分组徽章图片路径
     */
    groupBadgeImgPath?: string | null;
}
/**
 * 用户信息传输对象
 */
export interface UserDataDto {
    /**
     * 性别
     */
    sex?: GenderType;

    /**
     * 头像图片路径
     */
    avatarImage?: string | null;

    /**
     * 用户昵称
     */
    nickName?: string | null;

    /**
     * 生日
     */
    Birthday?:Date|null;
}


